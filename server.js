var socket = io('http://localhost:4000');

document.getElementById('btn').onclick = function(){
    Push.create('Notificación de prueba', {
        body: 'Esta es una notificación de prueba',
        icon: 'img/logo.png',
        timeout: 50000,
        vibrate: [100,100,100],
        onClick: function(){
            alert('click en la notificacion');
        }   
    });
}


let piedra = document.getElementById('piedra');
let papel = document.getElementById('papel');
let tijera = document.getElementById('tijera');



function idAleatorio(){
    var idAleatorio = Math.round(Math.random()*10000);
    return idAleatorio;
}

function Jugadores(myId, myNombre, idOponente, nombreOponente) {

  this.myId = myId;
  this.myNombre = myNombre;

  this.idOponente = idOponente;
  this.nombreOponente = nombreOponente;
}


function Oponente(id){
    var canal;
    var idOponente = id;
    var nombreOponente = $('#'+id).attr('data-nombre');

    var idRetador = $('#user').attr('data-id');
    var nombreRetador = $('#user').text();
    nombreRetador = nombreRetador.split(': ');
    nombreRetador = nombreRetador[1];
    var puntos = '';

    canal = idAleatorio();

    Swal.fire({
        title: 'Mensaje',
        text: "¿Deseas iniciar un juego con " + nombreOponente + "?",
        type: 'warning',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Enviar'
    }).then((result) => {
        if (result.value) {
            socket.emit('Invitacion', {
                idRetador: idRetador,
                nombreRetador: nombreRetador,
                idOponente: idOponente,
                nombreOponente: nombreOponente,
                nuevoCanal: canal,
            });

            $("#efecto").css("display", "block");
            document.getElementById('efecto').innerHTML ='<h1>Esperando la respuesta de ' + nombreOponente + '...</h1>';
            /*Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )*/
        }
    })


}


$('.opcionJugada').click(function(){
    var descripcion;
    var jugada = parseInt($(this).attr('data-jugada'));
    var idjugador = $('#user').attr('data-id');
    var jugador = $('.miUsuario').text();
    var canal = $('#canal').val();
    jugador = jugador.split(': ');
    jugador = jugador[1];

    switch (jugada) {
        case 1: descripcion = 'Piedra'; break;
        case 2: descripcion = 'Papel'; break;
        case 3: descripcion = 'Tijera'; break;
    }

    $('#jugadaHecha').html('');

    $('#jugadaHecha').append('<input type="text" id="idjugador" value="'+idjugador+'">' +
                             '<input type="text" id="jugador" value="'+jugador+'">' +
                             '<input type="text" id="jugada" value="'+jugada+'">' +
                             '<input type="text" id="descripcion" value="'+descripcion+'">');

    var dataOponente = $('#jugadaContrincante').html();

    socket.emit('Jugada', {  // Emitir el mensaje al servidor
       idjugador: idjugador,
       jugador:  jugador,
       jugada: jugada,
       descripcion: descripcion,
       canal: canal
    });

    $("#efecto").css("display", "block");
    document.getElementById('efecto').innerHTML = '<h1>Eperando el juego del oponente</h1>';

    if(dataOponente != ''){
        recibirJugadas();
    }

    // verificar si el oponente ya realizó

});


/* ENVIAR DATOS POR WEBSOCKETS */

$("#nombre").on('keyup', function(){
    document.getElementById('btn-registrar').disabled=false;
});


//Envía el nick para registrarse en el servidor
$('#btn-registrar').click(function(){
    $('#Cancelar').click();
    $("#jugadores-en-linea").css("display", "block");
    var nombreJugador = $('#nombre').val();
    socket.emit('nombreJugador', {nombreJugador: nombreJugador});
});



socket.on('miUsuario', function(data){
    console.log('El usuario está activo:',data);
    $('.miUsuario').append('<p id="user" data-id="'+data.id+'">Bienvenido: '+ data.nombre +'</p>');
    //$('#UsuarioActivo').text('Bienvenido: ' + data.nombre);
});

socket.on('listaUsuario', function(data){
  var jugadores = data.jugador;
  $("#jugadores-en-linea").html('');
  $("#jugadores-en-linea").append('<label for="">Lista de Jugadores conectados:</label>');
  for(var i = 0; i< jugadores.length; i++){
      $("#jugadores-en-linea").append(`<li>
                                        <span class="oponente" onclick='Oponente("${jugadores[i].id}");' id="${jugadores[i].id}" data-nombre="${jugadores[i].nombre}">
                                            ${jugadores[i].nombre}
                                        </span>
                                       </li>`);
    //$("#jugadores-en-linea").append('<li>' + jugadores[i] + '</li>');
  }
});

socket.on('recibirInvitacion', function(data) {
    var res;
    console.log(data.nombreRetador + ' retó a ' + data.nombreOponente);

    //res = confirm(data.nombreRetador + " te ha retado a un juego. ¿Aceptas?");
    Swal.fire({
        title: 'Mensaje de Invitación',
        text: data.nombreRetador + " te ha retado a un juego. ¿Aceptas?",
        type: 'warning',
        position: 'top',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Enviar'
    }).then((result) => {
        if (result.value) {
            socket.emit('decisionInvitacion', {
                idRetador: data.idRetador,
                nombreRetador: data.nombreRetador,
                idOponente: data.idOponente,
                nombreOponente: data.nombreOponente,
                nuevoCanal: data.nuevoCanal,
                res: true
            });

            // Unirme al nuevo canal de juego
            socket.emit('unirmeCanal', data);
        }
    })
})

socket.on('respuestaInvitacion', function(data) {
    $("#efecto").css("display", "none");
    document.getElementById('efecto').innerHTML = '';
    if (data.res == true) {
        Swal.fire({
            position: 'top',
            type: 'success',
            title: data.nombreOponente + ' aceptó el juego.',
            showConfirmButton: true
        })
        socket.emit('unirmeCanal', data);

    }else{
        Swal.fire({
            type: 'error',
            title: 'Mala suerte...',
            text: data.nombreOponente + ' rechazó el juego.'
        })
    }
})

/*socket.on('rechazaJuego', function(data) {
    $("#efecto").css("display", "none");
    document.getElementById('efecto').innerHTML = '';
    alert(data.nombreOponente + ' rechazó el juego.');
})*/

/*socket.on('aceptaJuego', function(data) {
    $("#efecto").css("display", "none");
    document.getElementById('efecto').innerHTML = '';
    alert(data.nombreOponente + ' aceptó el juego.');
    socket.emit('unirmeCanal', data);
})*/

socket.on('unirmeCanal', function(data) {
    $("#juego").css("display", "block");
    $('#juego').append('<p id="'+data.idOponente+'" style="display:none;">'+data.nombreOponente+'</p>');
    console.log('Se inició el Juego (Abre el Id Juego), canal: ' + data.nuevoCanal );
    $('.canal').html('');
    $('.canal').append('<input type="text" id="canal" value="'+ data.nuevoCanal +'">');
    $('.tipo-canal').text('N° Canal: ' + data.nuevoCanal);

    //alert(data.nombreOponente + ' aceptó el juego.');
    //socket.emit('unirmeCanal', data);
})

socket.on('resultadoOponente', function(data) {
    var idJugador = $('#idjugador').val();
    var nombreJugador = $('#jugador').val();
    var jugada = $('#jugada').val();
    var descripcion = $('#descripcion').val();

    var idOponente = data.idjugador;
    var nombreOponente = data.jugador;
    var jugadaOponente = data.jugada;
    var desOponente = data.descripcion;

    $('#jugadaContrincante').html('');

    $('#jugadaContrincante').append('<input type="text" id="idOponente" value="'+idOponente+'">' +
                             '<input type="text" id="nombreOponente" value="'+nombreOponente+'">' +
                             '<input type="text" id="jugadaOponente" value="'+jugadaOponente+'">' +
                             '<input type="text" id="desOponente" value="'+desOponente+'">');

    recibirJugadas();

    //console.log('Los datos del oponente son: ' + data.idjugador +', '+ data.jugador + ', ' +data.jugada);


})

function recibirJugadas(){

    console.log('ENTRÓ A LA COMPROBACIÓN DE JUEGOS...');

    // Data Retador
    var idJugador = $('#idjugador').val();
    var nombreJugador = $('#jugador').val();
    var jugada = $('#jugada').val();
    var descripcion = $('#descripcion').val();

    // Data Oponente
    var idOponente = $('#idOponente').val();
    var nombreOponente = $('#nombreOponente').val();
    var jugadaOponente = $('#jugadaOponente').val();
    var desOponente = $('#desOponente').val();

    // Lógica del juego
    if(jugada == 1){  //el usuario eligio piedra
        if(jugadaOponente == 1){
            document.getElementById('efecto').innerHTML ='<h1>¡Empate!</h1> <h3>Ambos eligieron piedra.</h3>';
            limpiarJugadas();
            setTimeout(salidaCanal, 2000);
        }else{
            if(jugadaOponente == 2){
                document.getElementById('efecto').innerHTML ='<h1>Perdiste!</h1> <h3>'+ nombreOponente +' eligió papel y tú piedra.</h3>';
                limpiarJugadas();
                setTimeout(salidaCanal, 2000);
            }else{
                if(jugadaOponente == 3){
                    document.getElementById('efecto').innerHTML ='<h1>Ganaste!</h1> <h3>'+ nombreOponente +' eligió tijera y tú piedra.</h3>';
                    limpiarJugadas();
                    setTimeout(salidaCanal, 2000);
                }
            }
        }
    }

    if(jugada == 2){  //el usuario eligio papel
        if(jugadaOponente == 1){
            document.getElementById('efecto').innerHTML ='<h1>Ganaste!</h1> <h3>'+ nombreOponente +' eligió piedra y tú papel.</h3>';
            limpiarJugadas();
            setTimeout(salidaCanal, 2000);
        }else{
            if(jugadaOponente == 2){
                document.getElementById('efecto').innerHTML ='<h1>¡Empate!</h1> <h3>Ambos eligieron papel.</h3>';
                limpiarJugadas();
                setTimeout(salidaCanal, 2000);
            }else{
                if(jugadaOponente == 3){
                    document.getElementById('efecto').innerHTML ='<h1>Perdiste!</h1> <h3>'+ nombreOponente +' eligió tijera y tú papel.</h3>';
                    limpiarJugadas();
                    setTimeout(salidaCanal, 2000);
                }
            }
        }
    }

    if(jugada == 3){  //el usuario eligio tijera
        if(jugadaOponente == 1){
              document.getElementById('efecto').innerHTML ='<h1>Perdiste!</h1> <h3>'+ nombreOponente +' eligió piedra y tú tijera.</h3>';
              limpiarJugadas();
              setTimeout(salidaCanal, 2000);
        }else{
            if(jugadaOponente == 2){
                document.getElementById('efecto').innerHTML ='<h1>Ganaste!</h1> <h3>'+ nombreOponente +' eligió papel y tú tijera.</h3>';
                limpiarJugadas();
                setTimeout(salidaCanal, 2000);
            }else{
                if(jugadaOponente == 3){
                    document.getElementById('efecto').innerHTML ='<h1>¡Empate!</h1> <h3>Ambos eligieron tijera.</h3>';
                    limpiarJugadas();
                    setTimeout(salidaCanal, 2000);
                }
            }
        }
    }



}

function limpiarJugadas(){
    $('#jugadaHecha').html('');
    $('#jugadaContrincante').html('');
    $('.canal').html('');
}

function quitarEfecto() {
    document.getElementById('efecto').style.display = "none";
}

function salidaCanal(){

    quitarEfecto();

    let timerInterval
    Swal.fire({
        title: 'Se cerrará este canal!',
        html: 'Se conectará al canal público en <strong></strong> segundos.',
        allowOutsideClick: false,
        timer: 5000,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                Swal.getContent().querySelector('strong')
                .textContent = (Swal.getTimerLeft() / 1000)
                .toFixed(0)
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        if (
            // Read more about handling dismissals
            result.dismiss === Swal.DismissReason.timer
        ) {
            console.log('Pasar a canal público')
        }
    })

    // Regresar a public
    setTimeout(regresoPublic, 5000);
}

function regresoPublic(){
    canalViejo = $('#canal').val();
    canalPublico = 'Público';

    socket.emit('volverPublic', {
        viejoCanal: canalViejo,
        canalNuevo: canalPublico
    });
}

socket.on('volverPublic', function(data) {
    $("#juego").css("display", "none");
    console.log('Se volvió a canal: ' + data);
    $('.tipo-canal').text('Canal: ' + data);
})
